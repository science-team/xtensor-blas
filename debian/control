Source: xtensor-blas
Priority: optional
Maintainer: Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper-compat (= 13),
 cmake (>= 3.1),
 libxtensor-dev (>= 0.24.0~),
 libblas-dev | libopenblas-dev | libblis-dev | libatlas-base-dev | libblas.so,
 liblapack-dev | libopenblas-dev | libatlas-base-dev | liblapack.so,
 googletest <!nocheck>,
 python3-breathe <!nodoc>,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
 sphinx-common,
 libjs-mathjax <!nodoc>
Standards-Version: 4.6.2
Section: libs
Homepage: https://github.com/xtensor-stack/xtensor-blas
Vcs-Browser: https://salsa.debian.org/science-team/xtensor-blas
Vcs-Git: https://salsa.debian.org/science-team/xtensor-blas.git
Rules-Requires-Root: no

Package: libxtensor-blas-dev
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends: libxtensor-dev (>= 0.24.0~),
 libopenblas-dev | libblis-dev | libatlas-base-dev | libblas-dev | libblas.so,
 libopenblas-dev | libatlas-base-dev | liblapack-dev | liblapack.so,
 ${misc:Depends}
Breaks: xtensor-blas-dev (<< 0.18.0-2~)
Replaces: xtensor-blas-dev (<< 0.18.0-2~)
Description: an extension to xtensor offering bindings to BLAS and LAPACK
 xtensor-blas is an extension to the header-only xtensor library,
 offering bindings to BLAS and LAPACK libraries through cxxblas and
 cxxlapack from the FLENS project.
 .
 xtensor-blas currently provides non-broadcasting dot, norm (1- and
 2-norm for vectors), inverse, solve, eig, cross, det, slogdet,
 matrix_rank, inv, cholesky, qr, svd in the xt::linalg namespace
 (check the corresponding xlinalg.hpp header for the function
 signatures). The functions, and signatures, are trying to be 1-to-1
 equivalent to NumPy. Low-level functions to interface with BLAS or
 LAPACK with xtensor containers are also offered in the blas and
 lapack namespace.

Package: xtensor-blas-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: libjs-mathjax,
 ${misc:Depends},
 ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Description: documentation for xtensor-blas
 xtensor-blas is an extension to the header-only xtensor library,
 offering bindings to BLAS and LAPACK libraries through cxxblas and
 cxxlapack from the FLENS project.
 .
 xtensor-blas currently provides non-broadcasting dot, norm (1- and
 2-norm for vectors), inverse, solve, eig, cross, det, slogdet,
 matrix_rank, inv, cholesky, qr, svd in the xt::linalg namespace
 (check the corresponding xlinalg.hpp header for the function
 signatures). The functions, and signatures, are trying to be 1-to-1
 equivalent to NumPy. Low-level functions to interface with BLAS or
 LAPACK with xtensor containers are also offered in the blas and
 lapack namespace.
 .
 This package provides documentation for xtensor-blas.
